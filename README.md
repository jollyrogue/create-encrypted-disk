# create-encrypted-disk

Scripts to create encrypted drives.

This is aimed at things like portable drives and secondary storage drives. Creating encrypted boot drives is out of scope.