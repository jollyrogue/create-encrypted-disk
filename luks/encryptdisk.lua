#!/usr/bin/env lua
--- encryptdisk.lua

-- TODO: Remove inspect as a dependency

--- START: Importing libraries
local inspect = require("inspect")
local pstat = require("posix.sys.stat")
local argparse = require("argparse")
--- END: Importing libraries

-- START: Variables
local device_score = 0
local skip_partitioning = false
-- END: Variables

-- START: Function definitions
-- checkBlockDevice checks to see if the passed string is a block device.
-- Returns: 0 for regular file, 1 for block device, 2 for partition
function checkBlockDevice(device)
  local stat_table = pstat.stat(device)
  local score = 0

  -- Check if the passed device is a block special file.
  -- Partitions and block devices will report as true.
  if pstat.S_ISBLK(stat_table["st_mode"]) == 1 then
    score = score + 1
   end

  -- Check is the passed device is a partition.
  -- Partitions will have a device type of 2081
  if stat_table["st_rdev"] == 2081 then
    score = score + 1
  end

  return score
end

-- Checks with the user to see if it's okay to continue.
function approve(prompt)
  io.write(prompt .. " (Y/N): ")
  local input = io.read("*line"):lower():sub(1,1)

  if input == 'y' then
    return true
  end

  return false
end

-- Trims the partition name back to the block device.
function getBlockDevice(partition)
  return partition:sub(1, -2)
end

-- Parse the parition information from parted
function parsePartInfo(part_line)
  local field_count = 0
  local part_info = {}

  for str in string.gmatch(part_line, "([^:;]*)") do
    if field_count == 0 then
      part_info["number"] = str
    elseif field_count == 1 then
      part_info["start"] = str
    elseif field_count == 2 then
      part_info["end"] = str
    elseif field_count == 3 then
      part_info["size"] = str
    elseif field_count == 4 then
      part_info["filesystem"] = str
    elseif field_count == 5 then
      part_info["name"] = str
    elseif field_count == 6 then
      part_info["flags"] = str
    end

    field_count = field_count + 1
  end

  return part_info
end
-- END: Function definitions

-- START: Script logic
-- Parsing arguments and options
local parser = argparse("encryptdisk.lua", "Script to encrypt a disk using LUKS.")
parser:argument("blockdevice", "Block device to encrypt.")
parser:argument("label", "Partition label.")
parser:option("-s --start", "Start of the partition.", "0%" )
parser:option("-e --end", "End of the partition.", "100%" )
local args = parser:parse()

io.write("Targeting " .. args["blockdevice"] .. " for encryption.\n")
io.write("Checking if " .. args["blockdevice"] .. " is a block device...\t")

device_score = checkBlockDevice(args["blockdevice"])

if device_score == 1 then
  io.write("SUCCESS! The target is a block device.\n")
elseif device_score == 2 then
  io.write("WARNING. The target is a partition.\n")

  skip_partitioning = approve("Skip drive partitioning and encrypt partition " .. args["blockdevice"] .. "?")

  if skip_partitioning == false then
    io.write("Trimming device name to get block device...")
    args["blockdevice"] = getBlockDevice(args["blockdevice"])
    io.write("\t" .. args["blockdevice"] .. "\n")
  end
else
  io.write("ERROR! The script only works on block devices.\n")
  os.exit(1)
end

io.write("Encrypting the device is a destructive process, and all data on "
         .. args["blockdevice"]
         ..  " will be destroyed.\n")
if approve("Continue?") == false then
  io.write("Quitting....\n")
  os.exit(0)
end

if skip_partitioning == true then
  -- Check if the parition is named correctly
  local block_dev = getBlockDevice(args["blockdevice"])
  local partition = {}

  local cmd_out = io.popen("sudo parted -sm " .. block_dev .. " print")
  -- Burning the first two lines since they aren't parition related
  cmd_out:read("*l")
  cmd_out:read("*l")

  for line in cmd_out:lines() do
    print(line)
    table.insert(partition, parsePartInfo(line))
  end

  print(inspect(partition))

  --[[
    parted -sm block_dev name partition args["label"]
  ]]
else
  -- Partition the drive.
  --[[
    parted -smfa optimal args["blockdevice"] mktable gpt
    parted -smfa optimal args["blockdevice"] mkpart offsite 0% 100%
  ]]
  local output = io.popen()
  print(output)
end

-- END: Script logic
